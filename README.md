# Conda Ansible Role

## But:
L'idée est de pouvoir "soumettre" des environnements conda via un repo git: https://192.168.103.4/taskforce-nncr/conda-env et laisser le CI et ansible les créer.

## Tasks:
- [x] Installer Miniconda
- [x] Upgrader ou downgrader Conda à la demande
